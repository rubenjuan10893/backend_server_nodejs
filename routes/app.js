var express = require("express");
var req_promise = require('request-promise');

var app = express();

// Ruta principal
app.get('/', (request, response, next) => {
    response.status(200).json({
        ok: true,
        mensaje: 'Petición get obtenida correctamente'
    });
});

app.get('/svg/:svg', (request, response) => {

	var svg = request.params.svg;
	
	console.log(`http://frontendtest.entradasatualcance.com/seatmap/svg/${ svg }`);

	req_promise({
		uri: `http://frontendtest.entradasatualcance.com/seatmap/svg/${ svg }`,
		json: false,
	}).then( resp => {
		response.status(200).send(resp);
	});
	
});

module.exports = app;
