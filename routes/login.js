var express = require('express');
var bcrypt = require('bcryptjs');
var jwt = require('jsonwebtoken');

var SEED = require('../config/config').SEED;
var CLIENT_ID = require('../config/config').CLIENT_ID;

var app = express();
var Usuario = require('../models/usuario');

// Google
const { OAuth2Client } = require('google-auth-library');
const client = new OAuth2Client(CLIENT_ID);

/* ********************************************************* */
/* ****** AUTENTICACIÓN NORMAL MEDIANTE TOKEN PROPIO ******* */
/* ********************************************************* */
app.post('/', (request, response) => {

    var body = request.body;

    Usuario.findOne({ email: body.email }, (err, usuarioObtenido) => {
        if (err) {
            return response.status(500).json({
                ok: false,
                mensaje: 'Error al buscar usuarios',
                errors: err
            });
        }

        if (!usuarioObtenido) {
            return response.status(400).json({
                ok: false,
                mensaje: 'Credenciales incorrectas - email',
                errors: err
            });
        }

        if (!bcrypt.compareSync(body.password, usuarioObtenido.password)) {
            return response.status(400).json({
                ok: false,
                mensaje: 'Credenciales incorrectas - password',
                errors: err
            });
        }

        // Si todo lo anterior está correcto, creamos el TOKEN
        usuarioObtenido.password = ':)';
        var token = jwt.sign({ usuario: usuarioObtenido }, SEED, { expiresIn: 14400 }) /* Expira en 4 horas */

        response.status(200).json({
            ok: true,
            mensaje: 'Login works!',
            id: usuarioObtenido._id,
            usuario: usuarioObtenido,
            token: token
        });
    });
});

/* *************************************************** */
/* ****** AUTENTICACIÓN NORMAL MEDIANTE GOOGLE ******* */
/* *************************************************** */
// Con la palabra async indicamos que la función devolverá una respuesta
// Con la palabra await indicamos que primero esperará una respuesta de la función
// y la almacenará en la constante
async function verify(token) {
    const ticket = await client.verifyIdToken({
        idToken: token,
        audience: CLIENT_ID, // Specify the CLIENT_ID of the app that accesses the backend
        // Or, if multiple clients access the backend:
        //[CLIENT_ID_1, CLIENT_ID_2, CLIENT_ID_3]
    }).catch(err => {
        return response.status(401).json({
            ok: false,
            message: 'El token no es correcto',
            errors: err
        })
    });

    // En el payload tenemos toda la información del usuario que quiere loguearse
    const payload = ticket.getPayload();
    // const userid = payload['sub'];
    // If request specified a G Suite domain:
    //const domain = payload['hd'];
    console.log('Payload Obtenido del Usuario', payload);
    return {
        nombre: payload.name,
        email: payload.email,
        img: payload.picture,
        google: true
    };
}

app.post('/google', async(request, response) => {
    var token = request.body.token;
    var googleUser = await verify(token)
        .catch(err => {
            return response.status(403).json({
                ok: false,
                mensaje: 'Token no válido.'
            });
        });

    Usuario.findOne({ email: googleUser.email }, (err, usuarioDB) => {
        if (err) {
            return response.status(500).json({
                ok: false,
                message: 'Error al buscar usuario en la base de datos',
                errors: err
            });
        }

        if (usuarioDB) {
            if (usuarioDB.google === false) {
                return response.status(400).json({
                    ok: false,
                    message: 'Debe de usar su autenticación normal'
                });
            } else {
                var token = jwt.sign({ usuario: usuarioDB }, SEED, { expiresIn: 14400 }); /* Expira en 4 horas */

                response.status(200).json({
                    ok: true,
                    mensaje: 'Login works!',
                    id: usuarioDB._id,
                    usuario: usuarioDB,
                    token: token
                });
            }
        } else {
        
        console.log('Google User', googleUser);
            // El usuario no existe en la base de datos, hay que crearlo
            var usuario = new Usuario();

            usuario.nombre = googleUser.nombre;
            usuario.email = googleUser.email;
            usuario.img = googleUser.img;
            usuario.google = true;
            usuario.password = ':)';
            

            usuario.save((err, usuarioGuardado) => {
                var token = jwt.sign({ usuario: usuarioGuardado }, SEED, { expiresIn: 14400 }); /* Expira en 4 horas */

                response.status(200).json({
                    ok: true,
                    mensaje: 'Usuario Logueado y Guardado en la base de datos!',
                    usuario: usuarioGuardado,
                    id: usuarioGuardado._id,
                    token: token
                });
            });
        }
    });
});

module.exports = app;
