var express = require("express");
var fileupload = require('express-fileupload');

// Importamos FileSistem
var fs = require('fs');

var app = express();

// Modelos
var Usuario = require('../models/usuario');
var Medico = require('../models/medico');
var Hospital = require('../models/hospital');

// Opciones por defecto
app.use(fileupload());

app.put('/:coleccion/:id', (request, response, next) => {
    var tipo = request.params.coleccion;
    var id = request.params.id;

    // tipos de coleccion
    var tiposValidos = ['hospitales', 'usuarios', 'medicos'];

    if (tiposValidos.indexOf(tipo) < 0) {
        return response.status(400).json({
            ok: false,
            mensaje: 'Tipo de colección no válida',
            errors: {
                message: 'Tipo de colección no válida'
            }
        });
    }

    if (!request.files) {
        return response.status(400).json({
            ok: false,
            mensaje: 'No se ha seleccionado ningun archivo',
            errors: {
                message: 'Se debe seleccionar un archivo'
            }
        });
    }

    // Empezamos las validaciones
    // Obtenemos el nombre del archivo
    var archivo = request.files.imagen;
    var nombreCortado = archivo.name.split('.');
    var extensionArchivo = nombreCortado[nombreCortado.length - 1];

    // Extensiones permitidas
    var extensionesValidas = ['png', 'jpg', 'gif', 'jpeg'];

    if (extensionesValidas.indexOf(extensionArchivo) < 0) {
        return response.status(400).json({
            ok: false,
            mensaje: 'Extensión no válida',
            errors: {
                message: 'Las extensiones válidas son los siguientes: ' + extensionesValidas.join(', ')
            }
        });
    }

    // Nombre de archivo personalizado
    // 'idUsuario'-'numeroRandom'.'extension-archivo'
    var nombreArchivo = `${ id }-${ new Date().getMilliseconds() }.${ extensionArchivo }`;

    // Mover el archivo del temporal a un path en especifico
    var path = `./uploads/${ tipo }/${ nombreArchivo }`;

    archivo.mv(path, err => {
        if (err) {
            return response.status(500).json({
                ok: false,
                mensaje: 'Error al mover el archivo',
                errors: err
            });
        }

        subirPorTipo(tipo, id, nombreArchivo, response);
    });
});

function subirPorTipo(tipoColeccion, id, nombreArchivo, response) {
    var pathViejo = '';
    switch (tipoColeccion) {
        case 'usuarios':
            Usuario.findById(id, (err, usuarioDB) => {
                if (err) {
                    return response.status(400).json({
                        ok: false,
                        message: `El usuario con el id: ${ id } en la Base de datos`,
                        errors: {
                            message: `No existe el usuario con id ${ id } en la Base de datos`
                        }
                    });
                }

                if (!usuarioDB) {
                    return response.status(500).json({
                        ok: false,
                        message: 'El usuario no existe en la base de datos',
                        errors: {
                            message: 'El usuario introducido no existe'
                        }
                    });
                }

                pathViejo = './uploads/usuarios/' + usuarioDB.img;

                // Si la imagen existe en el path viejo, se elimina
                if (fs.existsSync(pathViejo)) {
                    fs.unlinkSync(pathViejo);
                }

                usuarioDB.img = nombreArchivo;

                usuarioDB.save((err, usuarioActualizado) => {
                    if (err) {
                        return response.status(500).json({
                            ok: false,
                            mensaje: 'No se ha podido guardar el usuario en la Base de datos',
                            errors: err
                        });
                    }
                    return response.status(200).json({
                        ok: true,
                        mensaje: 'Imagen del usuario actualizada!',
                        usuario: usuarioActualizado
                    });
                });
            });
            break;

        case 'medicos':
            console.log('Estamos en los medicos');
            Medico.findById(id, (err, medicoDB) => {
                if (err) {
                    return response.status(400).json({
                        ok: false,
                        message: `El médico con el id: ${ id } en la Base de datos`,
                        errors: {
                            message: `No existe el médico con id ${ id } en la Base de datos`
                        }
                    });
                }

                if (!medicoDB) {
                    return response.status(500).json({
                        ok: false,
                        message: 'El médico no existe en la base de datos',
                        errors: {
                            message: 'El médico introducido no existe'
                        }
                    });
                }

                pathViejo = './uploads/medicos/' + medicoDB.img;

                if (fs.existsSync(pathViejo)) {
                    fs.unlinkSync(pathViejo);
                }

                medicoDB.img = nombreArchivo;

                medicoDB.save((err, medicoActualizado) => {
                    if (err) {
                        return response.status(500).json({
                            ok: false,
                            message: 'No se ha podido guardar el médico en la Base de datos',
                            errors: err
                        });
                    }

                    response.status(200).json({
                        ok: true,
                        message: 'Imágen del médico actualizada!',
                        medico: medicoActualizado
                    });
                });
            });
            break;

        case 'hospitales':
            Hospital.findById(id, (err, hospitalDB) => {
                if (err) {
                    return response.status(500).json({
                        ok: false,
                        message: 'A ocurrido un error con la conexión a la base de datos',
                        errors: err
                    });
                }

                if (!hospitalDB) {
                    return response.status(400).json({
                        ok: false,
                        message: `El hospital con el id: ${ id } en la Base de datos`,
                        errors: {
                            message: `No existe el hospital con id: ${ id } en la Base de datos`
                        }
                    });
                }

                pathViejo = `./uploads/hospitales/${ hospitalDB.img }`;

                if (fs.existsSync(pathViejo)) {
                    fs.unlinkSync(pathViejo);
                }

                hospitalDB.img = nombreArchivo;

                hospitalDB.save((err, hospitalActualizado) => {
                    if (err) {
                        return response.status(500).json({
                            ok: false,
                            message: 'No ha sido posible guardar el hospital en la Base de datos',
                            errors: {
                                message: 'No ha sido posible guardar el hospital en la Base de datos'
                            }
                        });
                    }

                    response.status(200).json({
                        ok: true,
                        message: 'Imágen del Hospital actualizada!',
                        hospital: hospitalActualizado
                    });
                });

            });
            break;

        default:
            break;
    }
}

module.exports = app;