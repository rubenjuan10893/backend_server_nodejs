// Importamos servidor express
var express = require('express');

// Importamos el middleware para la comprobación del token de la petición
var middleware = require('../middlewares/autenticacion');

// Inicializamos las variables
var app = express();
var Hospital = require('../models/hospital');

// Creamos las rutas

/* ******************************************* */
/* ****** OBTENER TODOS LOS HOSPITALES ******* */
/* ******************************************* */

app.get('/', (request, response) => {
    var desde = request.query.desde || 0;
    var limite = request.query.limite || 5;

    if (isNaN(desde)) {
        return response.status(400).json({
            ok: false,
            message: 'El valor introducido para el parametro "desde" no es correcto'
        });
    }

    if (isNaN(limite)) {
        return response.status(400).json({
            ok: false,
            message: 'El valor introducido como límite no es correcto'
        });
    }

    desde = Number(desde);
    limite = Number(limite);

    Hospital.find({})
        // Populate nos permite obtener toda la información de la tabla y que campos de la tabla para mostrar (funciona como una select)
        .populate('usuario', 'nombre email')
        .skip(desde)
        .limit(limite)
        .exec((err, hospitalesDB) => {
            console.log(hospitalesDB);
            if (err) {
                return response.status(500).json({
                    ok: false,
                    mensaje: 'Error cargando hospitales de la base de datos',
                    errors: err
                });
            }

            Hospital.count({}, (err, conteo) => {
                response.status(200).json({
                    ok: true,
                    total: conteo,
                    hospitales: hospitalesDB
                });
            });
        });
});

/* ******************************** */
/* ****** CREAR UN HOSPITAL ******* */
/* ******************************** */
app.post('/', middleware.verificaToken, (request, response) => {
    var body = request.body;

    var hospital = new Hospital({
        nombre: body.nombre,
        img: body.img,
        usuario: request.usuario._id
    });

    hospital.save((err, hospitalGuardado) => {
        if (err) {
            return response.status(400).json({
                ok: false,
                message: 'No se ha podido guardar el hospital en la base de datos',
                errors: err
            });
        }

        response.status(200).json({
            ok: true,
            message: 'Hospital Guardado!',
            hospital: hospitalGuardado
        });
    });
});

/* ************************************* */
/* ****** ACTUALIZAR UN HOSPITAL ******* */
/* ************************************* */
app.put('/:id', middleware.verificaToken, (request, response) => {
    var id = request.params.id;
    var body = request.body;

    Hospital.findById(id, (err, hospitalDB) => {
        if (err) {
            return response.status(500).json({
                ok: false,
                message: 'No se ha podido obtener ningun hospital con el id: ' + id,
                errors: err
            });
        }

        if (!hospitalDB) {
            return response.status(400).json({
                ok: false,
                message: 'No existe ningun hospital con el id: ' + id,
                errors: {
                    message: 'El hospital con el id: ' + id + ' no existe'
                }
            });
        }

        hospitalDB.nombre = body.nombre;
        hospitalDB.usuario = request.usuario._id;

        hospitalDB.save((err, hospitalDB) => {
            if (err) {
                return response.status(500).json({
                    ok: false,
                    message: 'No ha sido posible guardar el hospital en la base de datos',
                    errors: err
                });
            }

            response.status(200).json({
                ok: true,
                message: 'Hospital Actualizado!',
                hospital: hospitalDB
            });
        });
    });
});

/* ***************************************** */
/* ****** OBTENER UN HOSPITAL POR ID ******* */
/* ***************************************** */
app.get('/:id', middleware.verificaToken, (request, response) => {
    var id = request.params.id;

    Hospital.findById(id)
        .populate('usuario', 'nombre email')
        .exec((err, hospitalDB) => {
            if (err) {
                return response.status(500).json({
                    ok: false,
                    message: 'Ha ocurrido un error con la base de datos',
                    errors: err
                });
            }

            if (!hospitalDB) {
                return response.status(404).json({
                    ok: false,
                    message: `No existe ningun hospital con el id: ${ id }`
                });
            }

            response.status(200).json({
                ok: true,
                message: `Hospital con id: ${ id } encontrado!`,
                hospital: hospitalDB
            });
        });
});


/* ********************************* */
/* ****** BORRAR UN HOSPITAL ******* */
/* ********************************* */
app.delete('/:id', middleware.verificaToken, (request, response) => {
    var id = request.params.id;
    Hospital.findByIdAndRemove(id, (err, hospitalDB) => {
        if (err) {
            return response.status(500).json({
                ok: false,
                message: 'Ha ocurrido un error con la base de datos',
                errors: err
            });
        }

        if (!hospitalDB) {
            return response.status(400).json({
                ok: false,
                message: 'No se ha encontrado ningun hospital con el id: ' + id,
                errors: err
            });
        }

        response.status(200).json({
            ok: true,
            message: 'Hospital borrado correctamente!',
            hospital: hospitalDB
        });
    });
});

module.exports = app;