var express = require("express");

// Encriptacion de la contraseña mediante algoritmo de encriptación de una sola vía
var bcrypt = require('bcryptjs');

var middleware = require('../middlewares/autenticacion');

var app = express();

var Usuario = require('../models/usuario');


// Ruta usuarios

/* ***************************************** */
/* ****** OBTENER TODOS LOS USUARIOS ******* */
/* ***************************************** */

app.get('/', (request, response) => {

    var desde = request.query.desde || 0;
    var limite = request.query.limite || 5;

    if (isNaN(desde)) {
        return response.status(400).json({
            ok: false,
            message: 'El valor introducido para el parametro "desde" no es correcto'
        });
    }
    
    if (isNaN(limite)) {
    	return response.status(400).json({
    		ok: false,
    		message: 'El valor introducido como límite no es correcto'
    	});
    }

    desde = Number(desde);
    limite = Number(limite);

    Usuario.find({}, 'nombre email img role google')
        .skip(desde)
        .limit(limite)
        .exec(
            (err, usuariosDB) => {
                if (err) {
                    return response.status(500).json({
                        ok: false,
                        mensaje: 'Error cargando usuario de la base de datos',
                        errors: err
                    });
                }

                Usuario.count({}, (err, conteo) => {
                    response.status(200).json({
                        ok: true,
                        total: conteo,
                        usuarios: usuariosDB
                    });
                });

            });
});

/* ******************************************* */
/* ******* ACTUALIZAR UN NUEVO USUARIO ******* */
/* ******************************************* */

app.put('/:id', middleware.verificaToken, (request, response) => {
    var id = request.params.id;
    var body = request.body;

    Usuario.findById(id, (err, usuarioDB) => {
        if (err) {
            return response.status(500).json({
                ok: false,
                mensaje: 'Error al buscar usuario',
                errors: err
            });
        }

        if (!usuarioDB) {
            return response.status(400).json({
                ok: false,
                mensaje: 'Usuario con id: ' + id + ' no existe',
                errors: { message: 'No existe un usuario con ese ID' }
            });
        }

        usuarioDB.nombre = body.nombre;
        usuarioDB.email = body.email;
        usuarioDB.role = body.role;

        usuarioDB.save((err, usuarioGuardado) => {
            if (err) {
                return response.status(400).json({
                    ok: false,
                    mensaje: 'Error al actualizar el usuario',
                    errors: err
                });
            }

            response.status(200).json({
                ok: true,
                mensaje: 'Usuario actualizado correctamente',
                usuario: usuarioGuardado
            });
        });
    });
});

/* ************************************* */
/* ****** CREAR UN NUEVO USUARIO ******* */
/* ************************************* */

app.post('/', /* middleware.verificaToken */ (request, response) => {

    var body = request.body;

    console.log(body);

    if (body) {
        var usuario = new Usuario({
            nombre: body.nombre,
            email: body.email,
            password: bcrypt.hashSync(body.password, 10),
            img: body.img,
            role: body.role
        });

        console.log(usuario);
        
        usuario.save((err, usuarioGuardado) => {
        if (err) {
            return response.status(400).json({
                ok: false,
                mensaje: 'Error al crear un usuario',
                errors: err
            });
        }

		    response.status(201).json({
		        ok: true,
		        usuario: usuarioGuardado,
		        usuarioToken: request.usuario
		    });
		});
    } else {
    	response.status(404).json({
    		ok: false,
    		message: 'No se ha podido crear el usuario'
    	});
    }
});

/* ***************************************** */
/* ******* ELIMINAR UN USUARIO POR ID******* */
/* ***************************************** */

app.delete('/:id', middleware.verificaToken, (request, response) => {

    var id = request.params.id;
    Usuario.findByIdAndRemove(id, { useFindAndModify: false }, (err, usuarioBorrado) => {
        if (err) {
            return response.status(500).json({
                ok: false,
                mensaje: 'Error al eliminar el usuario',
                errors: err
            });
        }

        if (!usuarioBorrado) {
            return response.status(400).json({
                ok: false,
                mensaje: 'Usuario con id: ' + id + ' no existe',
                errors: { message: 'No existe un usuario con ese ID' }
            });
        }

        response.status(200).json({
            ok: true,
            mensaje: 'Usuario borrado correctamente!',
            usuario: usuarioBorrado
        })
    })
});

module.exports = app;
