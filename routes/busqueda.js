var express = require("express");

var app = express();
var Hospital = require('../models/hospital');
var Medico = require('../models/medico');
var Usuario = require('../models/usuario');
var regex;

/* ************************************************ */
/* ****** BÚSQUEDA ESPECIFICA EN LAS TABLAS ******* */
/* ************************************************ */

app.get('/coleccion/:tabla/:busqueda', (request, response) => {
    var tabla = request.params.tabla;
    var busqueda = request.params.busqueda;
    var promesa;
    regex = new RegExp(busqueda, 'i');

    switch (tabla.toLowerCase()) {
        case 'hospitales':
            promesa = buscarHospitales(regex);
            break;

        case 'medicos':
            promesa = buscarMedicos(regex);
            break;

        case 'usuarios':
            promesa = buscarUsuarios(regex);
            break;

        default:
            response.status(400).json({
                ok: false,
                message: 'La tabla no existe en la base de datos. Las tablas disponibles son: Usuarios, Medicos y Hospitales',
                error: { message: 'Tipo de tabla/coleccion no válido' }
            });
            break;
    }

    promesa.then(data => {
        response.status(200).json({
            ok: true,
            [tabla]: data
        });
    });
});

/* ********************************************* */
/* ****** BÚSQUEDA GENERAL EN LAS TABLAS ******* */
/* ********************************************* */

app.get('/todo/:busqueda', (request, response) => {
    var busqueda = request.params.busqueda;

    // creamos una expresion regular para poder realizar la búsqueda en la query
    // con el parametro 'i' estamos especificando que la expresión regular es insensible
    // tanto a mayusculas como minusculas y buscará coincidencias
    regex = new RegExp(busqueda, 'i');


    Promise.all([
            buscarHospitales(regex),
            buscarMedicos(regex),
            buscarUsuarios(regex)
        ])
        .then(respuestas => {
            var count = 0;

            respuestas.forEach((value, i, arr) => {
                count += arr[i].length;
            });

            response.status(200).json({
                ok: true,
                hospitales: {
                    values: respuestas[0],
                    totalHospitales: respuestas[0].length
                },
                medicos: {
                    values: respuestas[1],
                    totalMedicos: respuestas[1].length
                },
                usuarios: {
                    values: respuestas[2],
                    totalUsuarios: respuestas[2].length
                },
                total: count
            });
        })
        .catch(err => {
            return response.status(500).json({
                ok: false,
                message: 'Ha ocurrido un error al intentar mostrar los resultados',
                errors: err
            });
        });
});

function buscarHospitales(regex) {
    return new Promise((resolve, reject) => {
        Hospital.find({ 'nombre': regex })
            .populate('usuario', 'nombre email')
            .exec((err, hospitalesDB) => {
                if (err) {
                    reject('Error al cargar los hospitales', err);
                } else {
                    resolve(hospitalesDB, hospitalesDB.length);
                }
            });
    });
}

function buscarMedicos(regex) {
    return new Promise((resolve, reject) => {
        Medico.find({ 'nombre': regex })
            .populate('usuario', 'nombre email')
            .populate('hospital')
            .exec((err, medicosDB) => {
                if (err) {
                    reject('Error al cargar los medicos', err);
                } else {
                    resolve(medicosDB);
                }
            });
    });
}

function buscarUsuarios(regex) {
    return new Promise((resolve, reject) => {
        Usuario.find({ /* 'nombre': regex  */ }, 'nombre email role')
            .or([{
                'nombre': regex
            }, {
                'email': regex
            }])
            // .or({
            //     'email': regex
            // })
            .exec((err, usuariosDB) => {
                if (err) {
                    reject('Error al cargar los usuarios', err);
                } else {
                    console.log(usuariosDB);
                    resolve(usuariosDB);
                }
            });
    });
}

module.exports = app;