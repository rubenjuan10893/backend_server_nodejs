var express = require("express");

var app = express();

// la libreria path viene con node y nos permite crear un path de una forma sencilla
const path = require('path');

// FileSystem
const fs = require('fs');

app.get('/:coleccion/:img', (request, response) => {
    var coleccion = request.params.coleccion;
    var img = request.params.img;

    var pathImagen = path.resolve(__dirname, `../uploads/${ coleccion }/${ img }`);

    if (fs.existsSync(pathImagen)) {
        response.sendFile(pathImagen);
    } else {
        var pathNoImagen = path.resolve(__dirname, '../assets/no-img.jpg');
        response.sendFile(pathNoImagen);
    }
});

module.exports = app;