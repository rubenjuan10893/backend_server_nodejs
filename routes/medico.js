// Importar servidor express
var express = require('express');

// Importamos el middleware para la comprobación del token de la petición
var middleware = require('../middlewares/autenticacion');

// Inicializamos las variables
var app = express();
var Medico = require('../models/medico');

/* **************************************** */
/* ****** OBTENER TODOS LOS MEDICOS ******* */
/* **************************************** */
app.get('/', (request, response) => {
    var desde = request.query.desde || 0;
    var limite = request.query.limite || 5;

    if (isNaN(desde)) {
        return response.status(400).json({
            ok: false,
            message: 'El valor introducido para el parametro "desde" no es correcto'
        });
    }
    
    if (isNaN(limite)) {
    	return response.status(400).json({
    		ok: false,
    		message: 'El valor introducido como límite no es correcto'
    	});
    }

    desde = Number(desde);
    limite = Number(limite);

    Medico.find({})
        .populate('usuario', 'nombre email')
        .populate('hospital')
        .skip(desde)
        .limit(limite)
        .exec((err, medicosDB) => {
            if (err) {
                return response.status(500).json({
                    ok: false,
                    message: 'No se ha podido resolver la petición',
                    errors: err
                });
            }

            Medico.count({}, (err, conteo) => {
                response.status(200).json({
                    ok: true,
                    total: conteo,
                    medicos: medicosDB
                });
            });
        });
});

/* ****************************** */
/* ****** OBTENER UN MÉDICO ******* */
/* ****************************** */
app.get('/:id', (request, response) => {
	var id = request.params.id;
	
	Medico.findById( id )
			.populate('usuario', 'nombre email img')
			.populate('hospital')
			.exec((err, medicoDB) => {
				if (err) {
				    return response.status(500).json({
				        ok: false,
				        message: 'Error al intentar guardar en la base de datos',
				        errors: err
				    });
				}

				if (!medicoDB) {
					return response.status(400).json({
					    ok: false,
					    message: `El medico con el id ${ id } no existe`,
					    errors: { message: 'No existe un medico con ese ID' }
					});
				}
				
				response.status(200).json({
					ok: true,
					medico: medicoDB
				})
			})
});

/* ****************************** */
/* ****** CREAR UN MÉDICO ******* */
/* ****************************** */
app.post('/', middleware.verificaToken, (request, response) => {
    var body = request.body;

    console.log(request);

    var medico = new Medico({
        nombre: body.nombre,
        usuario: request.usuario._id,
        hospital: body.hospital
    });

    medico.save((err, medicoGuardado) => {
        if (err) {
            return response.status(500).json({
                ok: false,
                message: 'Error al intentar guardar en la base de datos',
                errors: err
            });
        }

        if (!medicoGuardado) {
            return response.status(400).json({
                ok: false,
                message: 'No se ha podido guardar el médico en la base de datos',
                errors: err
            });
        }

        response.status(200).json({
            ok: true,
            message: 'Médico guardado correctamente!',
            medico: medicoGuardado
        });
    });
});

/* *********************************** */
/* ****** ACTUALIZAR UN MEDICO ******* */
/* *********************************** */
app.put('/:id', middleware.verificaToken, (request, response) => {
    var id = request.params.id;
    var body = request.body;

    Medico.findById(id, (err, medicoDB) => {
        if (err) {
            return response.status(500).json({
                ok: false,
                message: 'Ha ocurrido un error con la base de datos',
                errors: err
            });
        }

        if (!medicoDB) {
            return response.status(400).json({
                ok: false,
                message: 'No se ha encontrado ningún médico con el id: ' + id,
                errors: err
            });
        }

        medicoDB.nombre = body.nombre;
        medicoDB.img = body.img;
        medicoDB.usuario = request.usuario._id;
        medicoDB.hospital = body.hospital;

        medicoDB.save((err, medicoGuardado) => {
            if (err) {
                return response.status(500).json({
                    ok: false,
                    message: 'Ha ocurrido un error con la base de datos',
                    errors: err
                });
            }

            response.status(200).json({
                ok: true,
                message: 'Médico actualizado!',
                medico: medicoGuardado
            });
        });
    });
});

/* ******************************* */
/* ****** BORRAR UN MEDICO ******* */
/* ******************************* */
app.delete('/:id', middleware.verificaToken, (request, response) => {
    var id = request.params.id;

    Medico.findByIdAndRemove(id, (err, medicoDB) => {
        if (err) {
            return response.status(500).json({
                ok: false,
                message: 'Ha ocurrido un error con la base de datos',
                errors: err
            });
        }

        if (!medicoDB) {
            return response.status(400).json({
                ok: false,
                message: 'No existe ningún médico con el id: ' + id,
                errors: err
            });
        }

        response.status(200).json({
            ok: true,
            message: 'Se ha borrado el médico correctamente!',
            medico: medicoDB
        });
    });
});

module.exports = app;
