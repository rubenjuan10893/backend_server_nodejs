// Requires (Importación de librerias de terceros o personalizadas)

var express = require('express');
//var mongoose = require('mongoose');
var bodyParser = require('body-parser');

// Inicializar variables

var port_server = 3000;
//var port_mongo = 27017;

//var uri_DDBB = `mongodb://localhost:${ port_mongo }/hospitalDB`;
var app = express();

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*"); // update to match the domain you will make the request from
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods", "POST, GET, PUT, DELETE, OPTIONS");
    next();
});

// BODY PARSER

/* parse application/x-www-form-urlencoded */
app.use(bodyParser.urlencoded({ extended: false }));
/* parse application/json */
app.use(bodyParser.json());

// Importar rutas

var appRoutes = require('./routes/app');
var loginRoutes = require('./routes/login');
var usuarioRoutes = require('./routes/usuario');
var hospitalRoutes = require('./routes/hospital');
var medicoRoutes = require('./routes/medico');
var busquedaRoutes = require('./routes/busqueda');
var uploadRoutes = require('./routes/upload');
var imagenesRoutes = require('./routes/imagenes');

// Escuchar peticiones express

app.listen(port_server, () => console.log(`Express server inizializado en el puerto ${ port_server }: \x1b[32m%s\x1b[0m`, 'online'));

// Conexión a la base de datos

// mongoose.connect(uri_DDBB, {
//     useNewUrlParser: true,
//     useUnifiedTopology: true
// }, (err, response) => {
//     if (err) throw err;

//     console.log(`Base de datos: \x1b[32m%s\x1b[0m`, 'online');
// });

// Rutas mediante middlewares
app.use('/login', loginRoutes);
app.use('/usuario', usuarioRoutes);
app.use('/hospital', hospitalRoutes);
app.use('/medico', medicoRoutes);
app.use('/busqueda', busquedaRoutes);
app.use('/upload', uploadRoutes);
app.use('/img', imagenesRoutes);
app.use('/', appRoutes);