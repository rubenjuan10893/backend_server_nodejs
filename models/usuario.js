// Para trabajar con mongoose y sus funciones hay que importarlo primero
var mongoose = require("mongoose");
var Schema = mongoose.Schema;

// Importar plugin de validaciones de mongoose
var uniqueValidator = require('mongoose-unique-validator');

// Roles válidos

var rolesValidos = {
    values: ['ADMIN_ROLE', 'USER_ROLE'],
    message: '{VALUE} no es un rol permitido'
};


// Definición del cuerpo del usuario
var usuarioSchema = new Schema({
    // Cada uno de los campos de la coleccion de nuestra base de datos debemos definirlos en este objeto de JavaScript
    // Y también podemos decirle de que tipo serán y que requerimientos va a tener mediante mongoose
    // type: "tipo" --> definimos el tipo de dato
    // required: [true o false, "mensaje que mostrará si falla"]
    nombre: { type: String, required: [true, 'El nombre es necesario'] },
    email: { type: String, unique: true, required: [true, 'El correo es necesario'] },
    password: { type: String, required: [true, 'La contraseña es necesaria'] },
    img: { type: String, required: false },
    role: { type: String, required: true, default: 'USER_ROLE', enum: rolesValidos },
    google: { type: Boolean, default: false }
});

// {PATH} --> Si tuviesemos mas campos que fuesen únicos, con {PATH} aparecería el nombre de la propiedad que sea única
usuarioSchema.plugin(uniqueValidator, { message: 'El {PATH} debe de ser único' });

module.exports = mongoose.model('Usuario', usuarioSchema);