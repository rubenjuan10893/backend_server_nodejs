// Importamos mongoose
var mongoose = require('mongoose')

// Creamos un Schema de mongoose
var Schema = mongoose.Schema;

var hospitalSchema = new Schema({
    nombre: { type: String, required: [true, 'El nombre del hospital es necesario'] },
    img: { type: String, required: false },
    // El tipo Schema.Types.ObjectID sirve para indicar a mongoose que el campo usuario
    // está relacionado con otra colección, y la referencia es Usuario. Al final este campo nos indicará
    // quien ha creado el registro
    usuario: { type: Schema.Types.ObjectId, ref: 'Usuario' }
}, {
    // mediante la propiedad collection indicamos el nombre que tendrá la colección en mongoDB
    collection: 'hospitales'
});


module.exports = mongoose.model('Hospital', hospitalSchema);