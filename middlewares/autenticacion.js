// Importamos JWT
var jwt = require('jsonwebtoken');

// Importamos nuestras configuraciones
var SEED = require('../config/config').SEED;

/* ************************************ */
/* ******* VERIFICAMOS EL TOKEN ******* */
/* ************************************ */

exports.verificaToken = function(request, response, next) {
    var token = request.query.token;

    jwt.verify(token, SEED, (err, decoded) => {

        // Si falla en este punto, todas las peticiones que estén por debajo de este middleware, dejarán de funcionar, ya que el token no es válido
        if (err) {
            return response.status(401).json({
                ok: false,
                mensaje: 'Token incorrecto!',
                errors: err
            });
        }

        // Decoded recoge el usuario que ha echo la petición
        request.usuario = decoded.usuario;
        next();
    });
};